const express = require('express');
const router = express.Router();
const { createTodo,
    updateTodo,
    getTodoList,
    clearTodo,
    deleteTodo
} = require('../controller/APITodoController');

// define the home page route
router.post('/createTodo', createTodo);
router.post('/updateTodo',updateTodo);
router.post('/todos', getTodoList);
router.post('/deleteTodo',deleteTodo);
router.post('/clearCompleted', clearTodo);

module.exports = router;