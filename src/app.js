const express = require('express')
const allRoutes = require('express-list-endpoints');

const router = require('./router')

const app = express()
const port = 3000

app.use('/api', router);
app.listen(port, () => {
    console.log(`Todo app listen on port ${port}!`);
    console.log('Registered Router: ');
    console.log(allRoutes(app));
});